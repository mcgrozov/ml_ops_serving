FROM ultralytics/ultralytics:latest-arm64

WORKDIR /app

COPY . .
RUN pip install --upgrade pip && pip install --no-cache-dir -r requirments.txt
EXPOSE 7860
ENV STREAMLIT_SERVER_PORT=8501

CMD ["python", "inference.py"]
