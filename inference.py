import os
import streamlit as st
import pandas as pd
import numpy as np
import mlflow
from catboost import CatBoostRegressor
from dotenv import find_dotenv, load_dotenv


class CatBoostWrapper:
    def __init__(self, model_name='catboost_model', model_stage='Production'):
        """
        To initialize the model
        model_name: Name of the model in registry
        model_stage: Stage of the model
        """
        load_dotenv(find_dotenv('.env'))
        registry_address = os.getenv('MLFLOW_TRACKING_URI')
        st.write(f'Loading the model from Registry: {registry_address}')
        mlflow.set_tracking_uri(registry_address)

        self.model = mlflow.pyfunc.load_model(
            model_uri=f"models:/{model_name}/{model_stage}"
        )


def predict(model_wrapper, input_data):
    """
    Make predictions using the loaded model
    """
    return model_wrapper.model.predict(input_data)


def launch_streamlit():
    st.title("CatBoost Model Inference")

    # Create a CatBoostWrapper instance
    model_wrapper = CatBoostWrapper()

    # Create input fields for features
    st.header("Input Features")
    feature1 = st.number_input("Feature 1", value=0.0)
    feature2 = st.number_input("Feature 2", value=0.0)
    feature3 = st.number_input("Feature 3", value=0.0)

    # Create a button to trigger prediction
    if st.button("Predict"):
        # Prepare input data
        input_data = pd.DataFrame({
            'feature1': [feature1],
            'feature2': [feature2],
            'feature3': [feature3]
        })

        # Make prediction
        prediction = predict(model_wrapper, input_data)

        # Display the prediction
        st.header("Prediction")
        st.write(f"The predicted value is: {prediction[0]:.2f}")


if __name__ == "__main__":
    launch_streamlit()
